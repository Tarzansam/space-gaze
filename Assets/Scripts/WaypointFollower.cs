using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollower : MonoBehaviour
{
    [SerializeField] private GameObject[] wayPoints;
    private int currentwWaypointIndex = 0;
    [SerializeField] private float speed = 2f;
    public Enemy enemyScript;
    public Enemy2 enemyScript2;
    [SerializeField] private bool wave1;
    [SerializeField] private bool wave2;
    void Update()
    {
        if (wave1)
        {

            if (enemyScript.waveState == WaveState.active)
            {
                if (Vector3.Distance(wayPoints[currentwWaypointIndex].transform.position, transform.position) < 0.1f)
                {
                    currentwWaypointIndex++;
                    if (currentwWaypointIndex >= wayPoints.Length)
                    {
                        currentwWaypointIndex = 0;
                    }
                }
                transform.position = Vector3.MoveTowards(transform.position, wayPoints[currentwWaypointIndex].transform.position, Time.deltaTime * speed);
            }
        }

        if (wave2)
        {

            if (enemyScript2.waveState == WaveState.active)
            {
                if (Vector3.Distance(wayPoints[currentwWaypointIndex].transform.position, transform.position) < 0.1f)
                {
                    currentwWaypointIndex++;
                    if (currentwWaypointIndex >= wayPoints.Length)
                    {
                        currentwWaypointIndex = 0;
                    }
                }
                transform.position = Vector3.MoveTowards(transform.position, wayPoints[currentwWaypointIndex].transform.position, Time.deltaTime * speed);
            }
        }
    }
}




