using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System;

public class MovementPlayer : MonoBehaviour
{
    [SerializeField] public float movementSpeed = 1.0f;
    [SerializeField] private GameObject bulletPrefab;
    private float bulletTimer;
    [SerializeField] private float interval;
    [SerializeField] public GameObject player;
    public int health;
    public bool dead;
    public Enemy enemyScript;
    public Enemy2 enemyScript2;
    public bool canShoot;
    //private float xRotation = 0f;


    private void Start()
    {
        //This is the time between each shot
        interval = 0.3f;
        player = GetComponent<GameObject>();
        health = 4;
        dead = false;
    }
    void Update()
    {
        if (enemyScript.waveState == WaveState.active && canShoot == true)
        {
            bulletTimer += Time.deltaTime;
        }
        float directionZ = Input.GetAxisRaw("Vertical");
        float directionX = Input.GetAxisRaw("Horizontal");
        transform.Translate(directionX * Time.deltaTime * movementSpeed, 0, 0);
        transform.Translate(0, 0, directionZ * Time.deltaTime * movementSpeed);

        if (transform.position.z <= -35)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -35);
        }
        if (transform.position.z >= -4)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -4);
        }
        if (transform.position.x >= 6.6)
        {
            transform.position = new Vector3(6.6f , transform.position.y, transform.position.z);
        }
        if (transform.position.x <= -6.6)
        {
            transform.position = new Vector3(-6.6f , transform.position.y, transform.position.z);
        }

        if (bulletTimer >= interval)
        {
            bulletTimer = 0;
            ShootBullet();
        }
        if (health <= 0)
        {
            dead = true;
            Destroy(gameObject);
        }

        if (Input.GetAxisRaw("Vertical") == 1 || Input.GetAxisRaw("Vertical") == -1)
        {
            interval = 0.5f;
        }

        if (Input.GetAxisRaw("Horizontal") == 1 || Input.GetAxisRaw("Horizontal") == -1)
        {
            interval = 0.5f;
        }

        if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
        {
            interval = 0.3f;
        }
    }
    private void ShootBullet()
    {
        Instantiate(bulletPrefab, transform.position, transform.rotation);
    }
}
