using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private Enemy enemy;
    [SerializeField] private Enemy2 enemy2;
    private MovementPlayer player;
    [SerializeField] private float speed;
    void Start()
    {
        Destroy(gameObject, 7f);
    }

    void Update()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && gameObject.CompareTag("PlayerBullet") || collision.gameObject.CompareTag("boss") && gameObject.CompareTag("PlayerBullet"))
        {
            Debug.Log("it colides");
            enemy = collision.GetComponent<Enemy>();
            if (FindObjectOfType<MovementPlayer>().canShoot == true)
            {
                enemy.enemyHealth--;
            }
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Enemy2") && gameObject.CompareTag("PlayerBullet"))
        {
            enemy2 = collision.GetComponent<Enemy2>();
            if (FindObjectOfType<MovementPlayer>().canShoot == true)
            {
                enemy2.enemyHealth--;
            }
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("Player") && gameObject.CompareTag("EnemyBullet"))
        {
            player = collision.GetComponent<MovementPlayer>();
            player.health--;
            Destroy(gameObject);
        }
    }
}
