using TMPro;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public enum WaveState
{
    spawning,
    active

}
public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    private float bulletTimer;
    public WaveState waveState;
    [SerializeField] private float interval;
    public int enemyHealth;
    private float bossTimer;
    private float bossInterval;
    [SerializeField] private int shotsFired;



    // Transforms to act as start and end markers for the journey.
    [SerializeField] private Transform startMarker;
    [SerializeField] private Transform endMarker;

    // Movement speed in units per second.
    [SerializeField] private float speed = 1.0F;

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLength;

    void Start()
    {
        interval = Random.Range(3f, 7f);
        waveState = WaveState.spawning;

        // Keep a note of the time the movement started.
        startTime = Time.time;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);

        enemyHealth = 7;

        if (gameObject.CompareTag("boss"))
        {
            enemyHealth = 30;
        }
        bossInterval = 3;
    }

    // Move to the target end position.
    void Update()
    {
        bulletTimer += Time.deltaTime;

        if (waveState == WaveState.spawning)
        {
            // Distance moved equals elapsed time times speed..
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fractionOfJourney);

            if (fractionOfJourney >= 1)
            {
                waveState = WaveState.active;
                FindObjectOfType<MovementPlayer>().canShoot = true;
            }
        }
        else
        {
            if (bulletTimer >= interval && gameObject.CompareTag("Enemy"))
            {
                bulletTimer = 0;
                ShootBullet();
            }


            bossInterval += Time.deltaTime;
            if (bossInterval >= 2f && gameObject.CompareTag("boss"))
            {

                if (bossTimer <= 15)
                {
                    bossTimer += Time.deltaTime;
                    if (bossTimer >= 0.2f)
                    {
                        ShootBullet();
                        bossTimer = 0;
                        shotsFired++;
                    }
                }
                else if(bossTimer >= 15f)
                {
                    bossInterval = 0;
                    shotsFired = 0;
                }
            }
        }



        if (enemyHealth == 0)
        {
            GameManager.score += 100;
            Destroy(gameObject);
        }
    }

    private void ShootBullet()
    {
        Instantiate(bulletPrefab, transform.position, transform.rotation);
    }
}
