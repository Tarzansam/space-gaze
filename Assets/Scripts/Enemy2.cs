using TMPro;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public enum WaveStateEnemy2
{
    spawning,
    active

}
public class Enemy2 : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab;
    private float bulletTimer;
    public WaveState waveState;
    [SerializeField] private float interval;
    public int enemyHealth;
    public GameManager manager;
    private GameObject[] allEnemiesWave2;


    // Transforms to act as start and end markers for the journey.
    [SerializeField] private Transform startMarker;
    [SerializeField] private Transform endMarker;

    // Movement speed in units per second.
    [SerializeField] private float speed = 1.0F;

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLength;

    void OnEnable()
    {
        FindObjectOfType<MovementPlayer>().canShoot = false;
        interval = Random.Range(3f, 7f);

        waveState = WaveState.spawning;

        // Keep a note of the time the movement started.
        startTime = Time.time;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);

        enemyHealth = 7;

        allEnemiesWave2 = GameObject.FindGameObjectsWithTag("Enemy2");

        foreach (GameObject gameObject in allEnemiesWave2)
        {
            gameObject.GetComponent<MeshCollider>().enabled = false;
        }

    }

    // Move to the target end position.
    void Update()
    {

        if (manager.enemyCount - 1 == 0)
        {
            bulletTimer += Time.deltaTime;
            Debug.Log("werkt");
        }

        if (waveState == WaveState.spawning && manager.enemyCount == 0)
        {
            foreach (GameObject gameObject in allEnemiesWave2)
            {
                if (gameObject.GetComponent<MeshCollider>() != null)
                    gameObject.GetComponent<MeshCollider>().enabled = true;
            }

            // Distance moved equals elapsed time times speed..
            float distCovered = (Time.time - startTime) * speed;

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            transform.position = Vector3.Lerp(startMarker.position, endMarker.position, fractionOfJourney);

            if (fractionOfJourney >= 1)
            {
                waveState = WaveState.active;
                FindObjectOfType<MovementPlayer>().canShoot = true;
            }
        }
        else
        {
            if (bulletTimer >= interval)
            {
                bulletTimer = 0;
                ShootBullet();
            }
        }

        if (enemyHealth <= 0)
        {
            GameManager.score += 100;
            Destroy(gameObject);
        }

    }

    private void ShootBullet()
    {
        Instantiate(bulletPrefab, transform.position, transform.rotation);
    }
}
