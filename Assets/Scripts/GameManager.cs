using TMPro;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private MovementPlayer player;
    [SerializeField] private Enemy enemy;
    [SerializeField] private TMP_Text scoreText;
    private int deathTimer;
    public static int score = 0;
    public List<GameObject> wave1 = new List<GameObject>();
    public List<GameObject> wave2 = new List<GameObject>();

    private GameObject[] enemies;
    public int enemyCount;

    private GameObject[] boss;
    public int bossCount;

    private bool wave2Active;

    int wave1Count = 0;

    void Start()
    {
        deathTimer = 300;
    }
    void Update()
    {
        wave1Count = wave1.Count;

        for (int i = 0; i < wave1.Count; i++)
        {
            if (wave1[i] == null)
                wave1Count--;

        }

        if (player.dead == true)
        {
            deathTimer--;
            if (deathTimer <= 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                score = 0;
            }
        }
        scoreText.text = "Score " + score.ToString();

        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        enemyCount = enemies.Length;
        Debug.Log(enemyCount);

        boss = GameObject.FindGameObjectsWithTag("boss");
        bossCount = boss.Length;
        Debug.Log(bossCount);

        if (enemyCount == 0 && bossCount == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        Debug.Log(enemyCount);
        if (wave2Active == false && wave1Count == 0)
        {
            wave2Active = true;
            ActivateNextWave();
        }


        wave1Count = 0;


    }

    public void ActivateNextWave()
    {
        if (enemyCount <= 0)
        {
            foreach (GameObject obj in wave2)
            {
                obj.SetActive(true);
                Debug.Log("it worky");
            }
        }
    }
}

