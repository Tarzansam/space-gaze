using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private MovementPlayer player;

    [SerializeField] private Image spriteRenderer;
    [SerializeField] private Sprite newSprite1;
    [SerializeField] private Sprite newSprite2;
    [SerializeField] private Sprite newSprite3;
    [SerializeField] private Sprite newSprite4;
    [SerializeField] private Sprite newSprite5;

    void Update()
    {
        if (player.health == 4)
        {
            spriteRenderer.sprite = newSprite1;
        }

        if (player.health == 3)
        {
            spriteRenderer.sprite = newSprite2;
        }

        if (player.health == 2)
        {
            spriteRenderer.sprite = newSprite3;
        }

        if (player.health == 1)
        {
            spriteRenderer.sprite = newSprite4;
        }

        if (player.health <= 0)
        {
            spriteRenderer.sprite = newSprite5;
        }
    }
}
